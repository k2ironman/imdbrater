
def _check_adjusted_rating(movie):
    """
    Checks the adjusted rating key. If not exists, inicializes it with the rating.
    """
    if 'Adjusted rating' not in movie.keys():
        movie['Adjusted rating'] = movie['Rating']
    return


def review_penalizer(movie, max_votes):
    """
    Sets adjusted rating according to the differance from the max votes
    """

    _check_adjusted_rating(movie)

    difference = max_votes - movie['Votes']
    decrease_val = 0.1 * int(difference / 100000)
    
    movie['Adjusted rating'] = round(movie['Adjusted rating'] - decrease_val, 1)
    return


def oscar_calculator(movie):
    """
    Sets adjusted rating according to number of oscars
    """

    _check_adjusted_rating(movie)

    if movie['Oscars'] != 0:
        if movie['Oscars'] in range(1, 3):
            reward = 0.3
        elif movie['Oscars'] in range(3, 6):
            reward = 0.5
        elif movie['Oscars'] in range(6, 11):
            reward = 1.0
        elif movie['Oscars'] > 10:
            reward = 1.5
        movie['Adjusted rating'] = round(movie['Adjusted rating'] + reward, 1)
    return
