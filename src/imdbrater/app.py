import sys
import json
import argparse
from prettytable import PrettyTable

try:
    from imdbrater.rating_adjuster import review_penalizer, oscar_calculator
    from imdbrater.scraper import getFirtsN
except:
    sys.path.append('./src')
    from imdbrater.rating_adjuster import review_penalizer, oscar_calculator
    from imdbrater.scraper import getFirtsN


def _display_movie_table(movies):
    """
    Display given data in table format
    """
    table = PrettyTable(['TITLE', 'RATING', 'VOTES', 'OSCARS', 'ADJUSTED RATING'])
    table.align = 'r'
    table.align['TITLE'] = 'l'
    for movie in movies:
        table.add_row(movie.values())
    print(table)


def _sort_by(movies, key):
    """
    Sort dictionaries by given key
    """
    movies.sort(reverse=True, key=lambda x: x[key])


def _save_json_file(movies, fileName):
    """
    Save movie data into file
    """
    out_file = open(fileName, "w")
    json.dump(movies, out_file, indent=4, ensure_ascii=False)


def main():
    # Setup argument parser
    parser = argparse.ArgumentParser(description="IMDb Rater")
    parser.add_argument('-n',
                        dest='n',
                        type=int,
                        help='Number of movies to calulate with')
    parser.add_argument('-f',
                        dest='file',
                        type=int,
                        help='Output JSON file name')

    # Set default values
    args = parser.parse_args()
    if args.n is None:
        args.n = 20
    if args.file is None:
        args.file = "result.json"

    # Read movie list from IMDb
    results = []
    for movie in getFirtsN(args.n):
        results.append(movie)
        print("Loading data. [{}/{}]".format(results.__len__(), args.n), end="\r")

    # Adjust ratings
    max_votes = max(results, key=lambda x: x['Votes'])['Votes']
    for movie in results:
        review_penalizer(movie, max_votes)
        oscar_calculator(movie)

    _sort_by(results, 'Adjusted rating')

    _display_movie_table(results)

    _save_json_file(results, args.file)


if __name__ == "__main__":
    main()
