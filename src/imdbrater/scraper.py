from bs4 import BeautifulSoup
import requests


def _getOscars(movie_url):
    """
    Receives url of the movie and returns the number of oscars
    """
    award_url = movie_url + "awards"
    soup = BeautifulSoup(requests.get(award_url).content, "html.parser")

    # Look for my span Oscar :)
    has_oscars = soup.find("span", string="Oscar", attrs={"class": "award_category"})
    if has_oscars:
        oscars = has_oscars.find_parent("table").find_all("td", attrs={"class": "award_description"})
        return oscars.__len__()
    else:
        return 0


def _getTitle(element):
    """
    Receives title tag and returns the title of the movie
    """
    return str(element.contents[0])


def _getMovieURL(element):
    """
    Receives title tag and returns the sub url of the movie
    """
    return element.attrs["href"]


def _getRating(element):
    """
    Receives rating tag and returns the rating
    """
    return float(element.contents[0])


def _getVotes(element):
    """
    Receives rating tag and returns the number of the votes
    """
    rating_text = element.attrs["title"]
    num_start = rating_text.find("on") + 3
    num_end = rating_text.find("user ratings") - 1
    return int(rating_text[num_start: num_end].replace(",", "")
    )


def _getTitleTag(row_data):
    """
    Receives row data and returns Tag containing title and movie url
    """
    return row_data.find("td", attrs={"class": "titleColumn"}).find("a")


def _getRatingTag(row_data):
    """
    Receives row data and returns Tag containing rating and votes
    """
    return row_data.find("td", attrs={"class": "imdbRating"}).find("strong")


def getFirtsN(n=20):
    """
    Get the first n movie from IMDb top 250 and returns a generator
    If parameter is not in [1-250] range uses default 20
    """
    if n not in range(1, 251):
        n = 20

    base_url = "https://www.imdb.com"
    top250_url = base_url + "/chart/top/"

    soup = BeautifulSoup(requests.get(top250_url).content, "html.parser")
    body = soup.find("table").findChild("tbody")
    row_data = body.find_next("tr")

    for _ in range(n):

        title_element = _getTitleTag(row_data)
        title = _getTitle(title_element)
        href = _getMovieURL(title_element)

        rating_element = _getRatingTag(row_data)
        rating = _getRating(rating_element)
        votes = _getVotes(rating_element)

        oscars = _getOscars(base_url + href)

        row_data = row_data.find_next("tr")

        yield {"Title": title, "Rating": rating, "Votes": votes, "Oscars": oscars}


if __name__ == "__main__":
    import time

    start_time = time.time()
    row = "{:<40}\t{:>10}\t{:>10}\t{:>10}"

    print(row.format("TITLE", "RATING", "VOTES", "OSCARS"))
    for element in getFirtsN(20):
        print(row.format(element["Title"], element["Rating"], element["Votes"], element["Oscars"]))

    print("Elapsed seconds: {:.2f}".format(time.time() - start_time))
