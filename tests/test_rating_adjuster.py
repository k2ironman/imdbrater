import sys
import unittest

try:
    from imdbrater.rating_adjuster import review_penalizer, oscar_calculator
except:
    sys.path.append('./src')
    from imdbrater.rating_adjuster import review_penalizer, oscar_calculator


class TestPenalizer(unittest.TestCase):

    def test_review_penalizer(self):
        sample1 = {
            "Rating": 8,
            "Votes": 100,
        }
        review_penalizer(sample1, 100)

        self.assertIsNotNone(sample1['Adjusted rating'])
        self.assertEqual(sample1['Adjusted rating'], sample1['Rating'])

        review_penalizer(sample1, 100 + 10**5)

        self.assertEqual(sample1['Adjusted rating'], sample1['Rating']-0.1)

        sample2 = {
            "Rating": 5,
            "Votes": 500,
            "Adjusted rating": 5
        }

        review_penalizer(sample2, sample2['Votes']+3*10**5)

        self.assertEqual(sample2['Adjusted rating'], sample2['Rating']-0.3)

    def test_oscar_calculator(self):
        sample1 = {
            "Rating": 8,
            "Oscars": 0,
        }

        oscar_calculator(sample1)

        self.assertIsNotNone(sample1['Adjusted rating'])
        self.assertEqual(sample1['Adjusted rating'], sample1['Rating'])

        sample2 = {
            "Rating": 8,
            "Oscars": 5,
        }

        oscar_calculator(sample2)

        self.assertEqual(sample2['Adjusted rating'], sample2['Rating'] + 0.5)

if __name__ == '__main__':
    unittest.main()
