# IMDb Rater

Solution for [The Big IMDB quest](https://datapao.notion.site/The-Big-IMDB-quest-54dae47fbe8b4e97b87dc60379f5c28d)

## Prepare
Install the dependency libraries.

```bash
pip3 install -r requirements.txt
```

## Start the app

### A. From source code

#### Run
```bash
python3 main.py
```
### B. Work as execution file

#### Build
The executable will be generated in `dist` folder.
```bash
pyinstaller --onefile src/imdbrater/app.py 
```

#### Run
```bash
cd dist
./imdbrater
```

### C. From pip package

### Install
```bash
python3 setup.py install
```

#### Run
```bash
imdbrater
```

## Startup arguments

| Name | Type | Default | Description |
| - | :-: | :-: | - |
| -n | Number | '20' | Number of movies to calculate with. Value should be `[1-250]` |
| -f | String | 'result.json' | Result file name |

