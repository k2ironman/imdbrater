from setuptools import find_packages, setup

PACKAGE_DESCRIPTION = 'Solution for The "Big IMDB quest"'

INSTALL_REQUIRES = [
    "beautifulsoup4",
    "prettytable"
]

setup(
    name="imdbrater",
    version="1.0",
    author="Dávid Kriston",
    author_email="davidk21ow@gmail.com",
    description=PACKAGE_DESCRIPTION,
    long_description=open("README.md").read(),
    long_description_content_type="text/markdown",
    url="https://bitbucket.org/k2ironman/imdbrater",
    python_requires=">=3.6",
    install_requires=INSTALL_REQUIRES,
    packages=find_packages("src"),
    package_dir={"": "src"},
    entry_points={
        'console_scripts': ['imdbrater=imdbrater.app:main', ]
    }
)
